﻿using DesafioApp.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace DesafioAppTests
{
    [TestClass]
    public class DesafioAppTest
    {
        private readonly string[] matriculaSemDV = { "1768", "0123" };
        private readonly string[] matriculasParaVerificar = { "1234-E", "3079-6"};

        private readonly IObjetoMatricula iObjetoMatricula;

        public DesafioAppTest()
        {
            iObjetoMatricula = new ControleMatricula();

        }

        [TestMethod]
        public void TestCalculaDV()
        {
            //Calcula digito verificador
            var itemCalculado =  iObjetoMatricula.CalcularDV(matriculaSemDV[0].ToString());
            Assert.AreEqual(itemCalculado, "1768-3");
        }

        [TestMethod]
        public void TestValidaDV()
        {
            // Validar digito verificador
            var itemValidado = iObjetoMatricula.ValidarDV(matriculasParaVerificar[0].ToString());
            Assert.AreEqual(itemValidado, "1234-E");
        }

    }
}
