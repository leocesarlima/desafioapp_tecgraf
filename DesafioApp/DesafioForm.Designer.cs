﻿namespace DesafioApp
{
    partial class DesafioForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbCarregaMatricula = new System.Windows.Forms.ListBox();
            this.btnCarregarArquivo = new System.Windows.Forms.Button();
            this.btnCalculaDV = new System.Windows.Forms.Button();
            this.btnValidarDV = new System.Windows.Forms.Button();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.rbCalculaDV = new System.Windows.Forms.RadioButton();
            this.rbValidarDV = new System.Windows.Forms.RadioButton();
            this.lbTrataMatricula = new System.Windows.Forms.ListBox();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.lblAviso = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbCarregaMatricula
            // 
            this.lbCarregaMatricula.FormattingEnabled = true;
            this.lbCarregaMatricula.Location = new System.Drawing.Point(12, 77);
            this.lbCarregaMatricula.Name = "lbCarregaMatricula";
            this.lbCarregaMatricula.Size = new System.Drawing.Size(308, 147);
            this.lbCarregaMatricula.TabIndex = 0;
            // 
            // btnCarregarArquivo
            // 
            this.btnCarregarArquivo.Location = new System.Drawing.Point(12, 48);
            this.btnCarregarArquivo.Name = "btnCarregarArquivo";
            this.btnCarregarArquivo.Size = new System.Drawing.Size(96, 23);
            this.btnCarregarArquivo.TabIndex = 1;
            this.btnCarregarArquivo.Text = "Carregar Arquivo";
            this.btnCarregarArquivo.UseVisualStyleBackColor = true;
            this.btnCarregarArquivo.Click += new System.EventHandler(this.btnCarregarArquivo_Click);
            // 
            // btnCalculaDV
            // 
            this.btnCalculaDV.Enabled = false;
            this.btnCalculaDV.Location = new System.Drawing.Point(12, 230);
            this.btnCalculaDV.Name = "btnCalculaDV";
            this.btnCalculaDV.Size = new System.Drawing.Size(75, 23);
            this.btnCalculaDV.TabIndex = 2;
            this.btnCalculaDV.Text = "Calcular DV";
            this.btnCalculaDV.UseVisualStyleBackColor = true;
            this.btnCalculaDV.Click += new System.EventHandler(this.btnCalcularDV_Click);
            // 
            // btnValidarDV
            // 
            this.btnValidarDV.Enabled = false;
            this.btnValidarDV.Location = new System.Drawing.Point(93, 230);
            this.btnValidarDV.Name = "btnValidarDV";
            this.btnValidarDV.Size = new System.Drawing.Size(75, 23);
            this.btnValidarDV.TabIndex = 3;
            this.btnValidarDV.Text = "Validar DV";
            this.btnValidarDV.UseVisualStyleBackColor = true;
            this.btnValidarDV.Click += new System.EventHandler(this.btnValidarDV_Click);
            // 
            // btnLimpar
            // 
            this.btnLimpar.Enabled = false;
            this.btnLimpar.Location = new System.Drawing.Point(93, 416);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(75, 23);
            this.btnLimpar.TabIndex = 4;
            this.btnLimpar.Text = "Limpar Tela";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // rbCalculaDV
            // 
            this.rbCalculaDV.AutoSize = true;
            this.rbCalculaDV.Checked = true;
            this.rbCalculaDV.Location = new System.Drawing.Point(12, 25);
            this.rbCalculaDV.Name = "rbCalculaDV";
            this.rbCalculaDV.Size = new System.Drawing.Size(81, 17);
            this.rbCalculaDV.TabIndex = 5;
            this.rbCalculaDV.TabStop = true;
            this.rbCalculaDV.Text = "Calcular DV";
            this.rbCalculaDV.UseVisualStyleBackColor = true;
            this.rbCalculaDV.CheckedChanged += new System.EventHandler(this.rbGerarDV_CheckedChanged);
            // 
            // rbValidarDV
            // 
            this.rbValidarDV.AutoSize = true;
            this.rbValidarDV.Location = new System.Drawing.Point(99, 25);
            this.rbValidarDV.Name = "rbValidarDV";
            this.rbValidarDV.Size = new System.Drawing.Size(75, 17);
            this.rbValidarDV.TabIndex = 6;
            this.rbValidarDV.Text = "Validar DV";
            this.rbValidarDV.UseVisualStyleBackColor = true;
            this.rbValidarDV.CheckedChanged += new System.EventHandler(this.rbValidarDV_CheckedChanged);
            // 
            // lbTrataMatricula
            // 
            this.lbTrataMatricula.FormattingEnabled = true;
            this.lbTrataMatricula.Location = new System.Drawing.Point(13, 263);
            this.lbTrataMatricula.Name = "lbTrataMatricula";
            this.lbTrataMatricula.Size = new System.Drawing.Size(307, 147);
            this.lbTrataMatricula.TabIndex = 7;
            // 
            // btnSalvar
            // 
            this.btnSalvar.Enabled = false;
            this.btnSalvar.Location = new System.Drawing.Point(13, 416);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(75, 23);
            this.btnSalvar.TabIndex = 8;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // lblAviso
            // 
            this.lblAviso.AutoSize = true;
            this.lblAviso.Location = new System.Drawing.Point(115, 53);
            this.lblAviso.Name = "lblAviso";
            this.lblAviso.Size = new System.Drawing.Size(0, 13);
            this.lblAviso.TabIndex = 9;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(174, 421);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(0, 13);
            this.lblInfo.TabIndex = 10;
            // 
            // DesafioForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 476);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.lblAviso);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.lbTrataMatricula);
            this.Controls.Add(this.rbValidarDV);
            this.Controls.Add(this.rbCalculaDV);
            this.Controls.Add(this.btnLimpar);
            this.Controls.Add(this.btnValidarDV);
            this.Controls.Add(this.btnCalculaDV);
            this.Controls.Add(this.btnCarregarArquivo);
            this.Controls.Add(this.lbCarregaMatricula);
            this.Name = "DesafioForm";
            this.Text = "Desafio Tecgraf";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbCarregaMatricula;
        private System.Windows.Forms.Button btnCarregarArquivo;
        private System.Windows.Forms.Button btnCalculaDV;
        private System.Windows.Forms.Button btnValidarDV;
        private System.Windows.Forms.Button btnLimpar;
        private System.Windows.Forms.RadioButton rbCalculaDV;
        private System.Windows.Forms.RadioButton rbValidarDV;
        private System.Windows.Forms.ListBox lbTrataMatricula;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Label lblAviso;
        private System.Windows.Forms.Label lblInfo;
    }
}

