﻿using System;

namespace DesafioApp.Classes
{
    public class ArquivoFactory
    {
        public IArquivoServico RecuperaObjeto(string funcFileName)
        {
            if (funcFileName.ToUpper().Contains("TXT"))
            {
                return new TextoArquivo(funcFileName);
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
