﻿using System;

namespace DesafioApp.Classes
{
    public class ControleMatricula : IObjetoMatricula
    {
        int digito;
        int multiplicador = 2;
        int valorTotal = 0;
        int restoDivisao = 0;

        public string CalcularDV(string pMatricula)
        {
            try
            {
                var matricula = Int32.Parse(pMatricula);

                while (matricula != 0)
                {
                    digito = matricula % 10;
                    matricula /= 10;
                    valorTotal += digito * multiplicador;
                    multiplicador++;
                }

                restoDivisao = valorTotal % 16;

                string hexValue = restoDivisao.ToString("X");

                return pMatricula + "-" + hexValue;
            }
            catch
            {
                throw;
            }
        }

        public string ValidarDV(string pMatricula)
        {
            try
            {
                var matricula = Int32.Parse(pMatricula.Substring(0, 4));

                while (matricula != 0)
                {
                    digito = matricula % 10;
                    matricula /= 10;
                    valorTotal += digito * multiplicador;
                    multiplicador++;
                }

                restoDivisao = valorTotal % 16;

                string hexValue = restoDivisao.ToString("X");

                return pMatricula.Substring(0, 4) + "-" + hexValue;
            }
            catch
            {
                throw;
            }
        }

    }
}
