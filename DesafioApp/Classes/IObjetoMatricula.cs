﻿
namespace DesafioApp.Classes
{
    public interface IObjetoMatricula
    {
        string ValidarDV(string pMatricula);
        string CalcularDV(string pMatricula);
    }
}
