﻿using System.IO;

namespace DesafioApp.Classes
{
    class TextoArquivo : IArquivoServico
    {
        private string _NomeArquivo;

        public TextoArquivo(string pNomeArquivo)
        {
            _NomeArquivo = pNomeArquivo;
        }

        public string[] MontaConteudo()
        {
            try
            {
                string[] linha = File.ReadAllLines(_NomeArquivo);
                return linha;
            }
            catch
            {
                throw;
            }
        }
    }
}
