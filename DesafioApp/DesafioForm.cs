﻿using DesafioApp.Classes;
using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace DesafioApp
{
    public partial class DesafioForm : Form
    {
        IArquivoServico iArquivoServico = null;
        IObjetoMatricula iObjetoMatricula = null;

        public DesafioForm()
        {
            InitializeComponent();
            ConfiguraTela();
        }

        private void btnCarregarArquivo_Click(object sender, EventArgs e)
        {
            try
            {
                //Ler arquivo txt
                var ofd = new OpenFileDialog() { Filter = "Text Documents|*.txt", Multiselect = false, ValidateNames = true };
                if (ofd.ShowDialog() != DialogResult.OK) return;

                //Define Factory
                var arquivoFactory = new ArquivoFactory();
                iArquivoServico = arquivoFactory.RecuperaObjeto(ofd.FileName);

                //Carrega conteudo do arquivo
                lbCarregaMatricula.DataSource = iArquivoServico.MontaConteudo();

                //Habilitar eventos da tela

                if (rbCalculaDV.Checked) btnCalculaDV.Enabled = true;
                else btnValidarDV.Enabled = true;
                btnLimpar.Enabled = true;

                lblAviso.Text = Path.GetFileName(ofd.FileName);
                lblAviso.ForeColor = Color.DarkGreen;

                MessageBox.Show("Documento carregado com sucesso!", "Mensagem de Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Mensagem", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCalcularDV_Click(object sender, EventArgs e)
        {

            try
            {
                //Limpa listBox antes de nova validacao
                lbTrataMatricula.Items.Clear();

                foreach (string item in lbCarregaMatricula.Items)
                {
                    iObjetoMatricula = new ControleMatricula();

                    //Calcular digito verificador
                    var itemCalculado = iObjetoMatricula.CalcularDV(item.ToString());

                    //Adiciona item validado ao listBox
                    lbTrataMatricula.Items.Add(itemCalculado);

                    btnSalvar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Mensagem de Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnValidarDV_Click(object sender, EventArgs e)
        {
            try
            {
                //Limpa listBox antes de nova validacao

                lbTrataMatricula.Items.Clear();

                foreach (string item in lbCarregaMatricula.Items)
                {
                    iObjetoMatricula = new ControleMatricula();

                    //Valida dígito verificador
                    var itemValidado = iObjetoMatricula.ValidarDV(item.ToString());

                    //Valida resultado do Digito verificador
                    string resultado = itemValidado == item.ToString() ? "Verdadeiro" : "Falso";
                    
                    //Adiciona item validadado ao listBox
                    lbTrataMatricula.Items.Add(itemValidado + "  " + resultado);

                    btnSalvar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Mensagem de Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                string caminho = rbCalculaDV.Checked ? "matriculasComDV.txt" : "matriculasVerificadas.txt";

                System.IO.StreamWriter SaveFile = new System.IO.StreamWriter(caminho);

                foreach (var item in lbTrataMatricula.Items)
                {
                    SaveFile.WriteLine(item.ToString());
                }

                SaveFile.ToString();
                SaveFile.Close();

                lblInfo.ForeColor = Color.DarkGreen;
                lblInfo.Text = "Arquivo salvo em bin/Debug";

                MessageBox.Show("Documento salvo com sucesso!", "Mensagem de Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Mensagem de Aviso", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            ConfiguraTela();
        }

        private void rbGerarDV_CheckedChanged(object sender, EventArgs e)
        {
            ConfiguraTela();
        }

        private void rbValidarDV_CheckedChanged(object sender, EventArgs e)
        {
            ConfiguraTela();
        }

        private void ConfiguraTela()
        {
            //Configuração default da tela

            lblAviso.Text = string.Empty;
            lblInfo.Text = string.Empty;

            lbCarregaMatricula.DataSource = null;
            lbTrataMatricula.DataSource = null;
            lbCarregaMatricula.Items.Clear();
            lbTrataMatricula.Items.Clear();

            btnCalculaDV.Enabled = false;
            btnValidarDV.Enabled = false;
            btnLimpar.Enabled = false;
            btnSalvar.Enabled = false;
        }
    }
}
